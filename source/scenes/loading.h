#pragma once

#include "../scene.h"

Scene* getLoadingScene(Scene* next_scene, void(*func)(void));